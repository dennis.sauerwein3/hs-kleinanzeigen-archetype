# HS Kleinanzeigen Archetype

[Maven Archetype](https://maven.apache.org/guides/introduction/introduction-to-archetypes.html) zur Erstellung einer simplen Spring Boot Anwendung für HS Kleinanzeigen.

## Voraussetzungen
* JDK 11
* git
* maven


## Generierung
* Zielanwendung via cli generieren:

```bash
$ mvn archetype:generate \
    -DarchetypeGroupId=io.github.leichtundkross \
    -DarchetypeArtifactId=hs-kleinanzeigen-archetype \
    -DarchetypeVersion=1.0.1 \
    -DgroupId=de.hs.da \
    -DartifactId=hs-kleinanzeigen \
    -Dversion=0.0.1-SNAPSHOT \
    -DinteractiveMode=false
```

oder 

* Zielanwendung mit IntelliJ generieren:

    * **File->New->Project**

    * Check "Create from archetype" box

    * Entweder **io.github.leichtundkross:hs-kleinanzeigen-archetype** aus der Liste auswählen oder hinzufügen. 
ArchetypeGroupId=**io.github.leichtundkross**, DarchetypeArtifactId=**hs-kleinanzeigen-archetype**, version=**1.0.1**.
    * GroupId=**de.hs.da**, artifactId=**hs-kleinanzeigen**, version=**0.0.1-SNAPSHOT**.

